//declare var API_URL: string
var API_URL = 'http://localhost:5000'
import 'whatwg-fetch'

interface ApiData {
  [key: string]: string | number | ApiData
}

type HttpMethod = 'GET' | 'POST' | 'PUT' | 'DELETE'


const apiUrl = (path: string) => {
  return API_URL + path
}

async function request(
  url: string,
  method: HttpMethod = 'GET',
  body?: any,
  headers?: Headers
) {
  headers = headers || new Headers({
    ContentType: 'application/json'
  })
  const response = await fetch(url, { body, method, headers })
  if (response.ok) {
    const json = await response.json()
    return json
  }
  console.error(await response.json())
}

async function get(path: string) {
  return request(apiUrl(path), 'GET')
}

async function put(path: string, data: ApiData) {
  return request(apiUrl(path), 'PUT', data)
}

async function post(path: string, data: ApiData) {
  return request(apiUrl(path), 'POST', data)
}

async function upload(path: string, file: any) {
  // do not set content-type header
  const headers = new Headers()
  const body = new FormData()
  body.append('file', file)
  return request(apiUrl(path), 'POST', body, headers)
}

export default {
  get,
  put,
  post,
  upload
}
