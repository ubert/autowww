export default interface Session {
  session_id: string
  name: string
  filename: string
}
