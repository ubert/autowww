import http from 'lib/http'
import React from 'react'
import FileDrop from 'components/FileDrop'

import Session from 'interfaces/Session'

interface Props {
}

interface State {
  files: Array<any>
  title: string
  message?: string
  ws?: WebSocket
}

class Application extends React.Component<Props, State> {

  protected socket = new WebSocket('ws://localhost:5000/echo')

  constructor(props: Props) {
    super(props)
    this.state = {
      files: [],
      title: 'Audio!',
    }
  }

  public componentDidMount() {
    this.connect()
  }

  public render() {
    return (
      <div className="container">
        <h1>{ this.state.title }</h1>
        <menu>Menu</menu>
        <main>
          <FileDrop
            onDrop={this.handleUpload.bind(this)}
            accept="audio/wav"
          />
        </main>
        <input onChange={this.handleInput.bind(this)} />
      </div>
    )
  }

  private connect() {
    const ws = new WebSocket('ws://localhost:5000/echo')
    ws.onopen = () => {
      console.log('websocket open')
      this.setState({ws})
    }
    ws.onclose = () => {
      console.log('websocket closed')
    }
    ws.onerror = e => {
      console.error(e)
    }
    ws.onmessage = message => {
      this.handleMessage(message)
    }
  }

  private handleUpload(files: Array<any>) {
    const results = files.map(f => http.upload('/upload', f))
    if (results.length === 1) {
      results[0].then((session: Session) => {
        const title = session.name
        this.setState({title})
      }).catch((error) => console.error(error))
    }
    else if (results.length > 1) {
      console.error('too much file')
    }
    else {
      console.error('no file')
    }
  }

  private handleData(data: any) {
//    const result = JSON.parse(data);
//    console.log(result)
    console.log(data)
  }
  private handleInput(ev: React.FormEvent<HTMLInputElement>) {
    const message = ev.currentTarget.value
    const { ws } = this.state
    if (ws && ws.readyState !== WebSocket.CLOSED) {
        ws.send(message)
    }
  }
  private handleMessage(ev: any) {
      console.log(ev)
      this.handleData(ev.data)
  }
}

export default Application
