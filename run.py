#!/usr/bin/env python3
import os
from app import create_app

hostname = '127.0.0.1'
port     = 5000

os.environ['APP_ENV'] = 'config.Config'
os.environ['FLASK_ENV'] = 'Development'


if __name__ == "__main__":
    from gevent.pywsgi import WSGIServer
    from geventwebsocket.handler import WebSocketHandler
    app = create_app()
    http_server = WSGIServer((hostname, port), app, handler_class=WebSocketHandler)
    http_server.serve_forever()
