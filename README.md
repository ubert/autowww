Web interface for smp_audio
===========================

Setup
-----

first:
A python3 environment with pip, i suggest setting up a virtualenv.
A javacript environment with npm

then:
git submodule --init
pip install -r requirements
pip install -r dev-requirements
npm i
npm run build
flask run