def handle_echo(ws):
    while not ws.closed:
        message = ws.receive()
        ws.send(message)
