# -*- coding: utf-8 -*-
import os
from flask import Flask
from flask_sockets import Sockets
from . import views
from . import events


# determine config and secret file
try:
    APP_ENV = os.environ['APP_ENV']
except KeyError:
    APP_ENV = 'config.Config'

try:
    SECRETS = os.environ['SECRETS']
except KeyError:
    os.environ['SECRETS'] = '../secrets'


def setup_routes(app):
    views.upload.methods = ['POST']
    app.add_url_rule('/', 'dashboard', views.dashboard)
    app.add_url_rule('/upload', 'upload', views.upload)
    app.add_url_rule('/get/<session_id>', 'get', views.get)


def setup_sockets(sockets):
    sockets.add_url_rule('/echo', None, f=events.handle_echo)


def create_app():
    """
    Using the app-factory pattern
    :return Flask
    """
    app = Flask(
        __name__,
        static_folder='static',
        template_folder='templates'
    )
    app.config.from_object(APP_ENV)
    app.config.from_envvar('SECRETS', silent=True)
    setup_routes(app)
    sockets = Sockets(app)
    setup_sockets(sockets)
    return app
