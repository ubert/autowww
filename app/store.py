from flask import current_app
import json
import os
import uuid
from simplekv.fs import FilesystemStore


class Session(object):

    exports = (
        'name',
        'filename'
    )

    def __init__(self, session_id=None, session_dir=None):
        if session_id is None:
            self.session_id = str(uuid.uuid4())
            os.mkdir(self.path(session_dir))
        else:
            self.session_id = session_id

        self._store = FilesystemStore(self.path(session_dir))

    def path(self, session_dir=None):
        session_dir = session_dir or current_app.config['SESSIONS_DIR']
        return os.path.join(session_dir, self.session_id)

    def put(self, key, value, encoding='utf-8'):
        self._store.put(key, value.encode(encoding=encoding))

    def get(self, key, encoding='utf-8'):
        return self._store.get(key).decode(encoding=encoding)

    def from_dict(self, data):
        for k, v in data.iter():
            self.put(k, v)

    def to_dict(self):
        rv = {k: self.get(k) for k in self.exports}
        rv['session_id'] = self.session_id
        return rv

    def to_json(self):
        return json.dumps(self.to_dict())
