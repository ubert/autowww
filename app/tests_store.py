from app import create_app
from flask_testing import TestCase
from app.store import Session


class StoreTestCase(TestCase):

    def create_app(self):
        return create_app()

    def test_store_retrieve_value(self):
        test = 'test'
        session = Session()
        session.put('test', test)
        assert session.get('test') == test
