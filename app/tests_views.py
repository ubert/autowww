from app import create_app
from flask_testing import TestCase


class ViewsTestCase(TestCase):

    def create_app(self):
        app = create_app()
        self.client = app.test_client()
        return app

    def test_root(self):
        self.assert200(self.client.get('/'))
