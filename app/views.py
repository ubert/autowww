# -*- coding: utf-8 -*-
import json
import os
from flask import (
    current_app,
    render_template,
    request,
    Response,
)
from werkzeug import secure_filename
from .store import Session


def dashboard() -> str:
    return render_template('main.html')


def upload() -> Response:
    """
    Uploaded File is the first step
    """
    try:
        audio_file = request.files['file']
        filename = secure_filename(audio_file.filename)
        name = filename
        session = Session()
        session.put('name', name)
        session.put('filename', filename)
        audio_file.save(
            os.path.join(session.path(), 'audiofile')
        )
    except KeyError:
        return Response(json.dumps({"message": "no file"}), 400)
    else:
        return Response(session.to_json(), 201)


def get(session_id: str) -> Response:
    for d in os.scandir(current_app.config['SESSIONS_DIR']):
        if d == session_id:
            session = Session(session_id)
            return Response(session.to_json(), 200)
    return Response(json.dumps({"message": "not found"}), 404)
