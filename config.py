class Config(object):
    DEBUG = True
    DEVELOPMENT = True
    SECRET_KEY = 'secret'
    FLASK_SECRET = SECRET_KEY
    SESSIONS_DIR = 'app/sessions'


class TestingConfig(Config):
    TESTING = True
