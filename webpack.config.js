const path = require('path')

module.exports = (env, args) => {
  return { 
    entry: {
      main: './app-js/main.jsx'
    },
    output: {
      filename: '[name].bundle.js',
      chunkFilename: '[name].bundle.js',
      path: __dirname + '/app/static/scripts',
      publicPath: '/scripts/'
    },
    devtool: 'source-map',
    resolve: {
      extensions: ['.js', '.jsx', '.json', '.ts', '.tsx'],
      modules: [path.resolve(__dirname, 'app-js'), 'node_modules'],
      alias: {
        deepmerge$: path.resolve(
            __dirname,
            'node_modules/deepmerge/dist/umd.js'
        )
      }

    },
    module: {
      rules: [
        {
          test: /\.(js|jsx)$/,
          exclude: /node_modules/,
          use: {
            loader: "babel-loader"
          }
        },
        {
          test: /\.(ts|tsx)$/,
          exclude: /node_modules/,
          use: ["babel-loader", "ts-loader"]
        }
      ]
    }
  }
}
